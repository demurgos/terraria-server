#!/bin/bash

# The actual program name
declare -r myname="terrariad"
declare -r game="terraria"

# General rule for the variable-naming-schema:
# Variables in capital letters may be passed through the command line others not.
# Avoid altering any of those later in the code since they may be readonly (IDLE_SERVER is an exception!)

# You may use this script for any game server of your choice, just alter the config file
[[ -n "${SERVER_ROOT}" ]]  && declare -r SERVER_ROOT=${SERVER_ROOT}   || SERVER_ROOT="/srv/${game}"
[[ -n "${BACKUP_DEST}" ]]  && declare -r BACKUP_DEST=${BACKUP_DEST}   || BACKUP_DEST="/srv/${game}/backup"
[[ -n "${BACKUP_PATHS}" ]] && declare -r BACKUP_PATHS=${BACKUP_PATHS} || BACKUP_PATHS="world"
[[ -n "${BACKUP_FLAGS}" ]] && declare -r BACKUP_FLAGS=${BACKUP_FLAGS} || BACKUP_FLAGS="-z"
[[ -n "${KEEP_BACKUPS}" ]] && declare -r KEEP_BACKUPS=${KEEP_BACKUPS} || KEEP_BACKUPS="10"
[[ -n "${GAME_USER}" ]]    && declare -r GAME_USER=${GAME_USER}       || GAME_USER="terraria"
[[ -n "${MAIN_EXECUTABLE}" ]] && declare -r MAIN_EXECUTABLE=${MAIN_EXECUTABLE} || MAIN_EXECUTABLE="server/TerrariaServer.exe"
[[ -n "${SESSION_NAME}" ]] && declare -r SESSION_NAME=${SESSION_NAME} || SESSION_NAME="${game}"

# Command and parameter declaration with which to start the server
[[ -n "${SERVER_START_CMD}" ]] && declare -r SERVER_START_CMD=${SERVER_START_CMD} || SERVER_START_CMD="mono './${MAIN_EXECUTABLE}' -config '${SERVER_ROOT}/serverconfig.txt'"

# System parameters for the control script
[[ -n "${IDLE_SERVER}" ]]       && tmp_IDLE_SERVER=${IDLE_SERVER}   || IDLE_SERVER="true"
[[ -n "${IDLE_SESSION_NAME}" ]] && declare -r IDLE_SESSION_NAME=${IDLE_SESSION_NAME} || IDLE_SESSION_NAME="idle_server_${SESSION_NAME}"
[[ -n "${GAME_PORT}" ]]         && declare -r GAME_PORT=${GAME_PORT}       || GAME_PORT="7777"
[[ -n "${CHECK_PLAYER_TIME}" ]] && declare -r CHECK_PLAYER_TIME=${CHECK_PLAYER_TIME} || CHECK_PLAYER_TIME="30"
[[ -n "${IDLE_IF_TIME}" ]]      && declare -r IDLE_IF_TIME=${IDLE_IF_TIME} || IDLE_IF_TIME="60"

# Additional configuration options which only few may need to alter
[[ -n "${GAME_COMMAND_DUMP}" ]] && declare -r GAME_COMMAND_DUMP=${GAME_COMMAND_DUMP} || GAME_COMMAND_DUMP="/tmp/${myname}_${SESSION_NAME}_command_dump.txt"

# Variables passed over the command line will always override the one from a config file
source /etc/conf.d/"${game}" 2>/dev/null || >&2 echo "Could not source /etc/conf.d/${game}"

# Preserve the content of IDLE_SERVER without making it readonly
[[ -n ${tmp_IDLE_SERVER} ]] && IDLE_SERVER=${tmp_IDLE_SERVER}


# Strictly disallow uninitialized Variables
set -u
# Exit if a single command breaks and its failure is not handled accordingly
set -e

# Check whether sudo is needed at all
if [[ "$(whoami)" == "${GAME_USER}" ]]; then
	SUDO_CMD=""
else
 	SUDO_CMD="sudo -u ${GAME_USER}"
fi

# Choose which flavor of netcat is to be used
if command -v netcat &> /dev/null; then
	NETCAT_CMD="netcat"
elif command -v ncat &> /dev/null; then
	NETCAT_CMD="ncat"
else
	NETCAT_CMD=""
fi

# Check for sudo rigths
if [[ "$(${SUDO_CMD} whoami)" != "${GAME_USER}" ]]; then
	>&2 echo -e "You have \e[39;1mno permission\e[0m to run commands as $GAME_USER user."
	exit 21
fi

# Pipe any given argument to the game server console,
# sleep for $sleep_time and return its output if $return_stdout is set
game_command() {
	if [[ -z "${return_stdout:-}" ]]; then
		${SUDO_CMD} screen -S "${SESSION_NAME}" -X stuff "$(printf "%s\r" "$*")"
	else
		${SUDO_CMD} screen -S "${SESSION_NAME}" -X log on
		${SUDO_CMD} screen -S "${SESSION_NAME}" -X stuff "$(printf "%s\r" "$*")"
		sleep "${sleep_time:-0.3}"
		${SUDO_CMD} screen -S "${SESSION_NAME}" -X log off
		${SUDO_CMD} cat "${GAME_COMMAND_DUMP}"
		${SUDO_CMD} rm "${GAME_COMMAND_DUMP}"
	fi
}

# Check whether there are player on the server through playing
is_player_online() {
	response="$(sleep_time=0.6 return_stdout=true game_command playing)"
	# Delete leading line and free response string from fancy characters
	response="$(echo "${response}" | sed -r -e 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})*)?[JKmsuG]//g')"
	# The playing command prints `No players connect.` if there are not players: check for it.
	if [[ $(echo "${response}" | grep "No players connected." &> /dev/null) ]]; then
		# No player is online
		return 0
	else
		# A player is online (or it could not be determined)
		return 1
	fi
}

# Check whether the server is visited by a player otherwise shut it down
idle_server_daemon() {
	# This function is run within a screen session of the GAME_USER therefore SUDO_CMD can be omitted
	if [[ "$(whoami)" != "${GAME_USER}" ]]; then
		>&2 echo "Somehow this hidden function was not executed by the ${GAME_USER} user."
		>&2 echo "This should not have happend. Are you messing around with this script? :P"
		exit 22
	fi

	# Time in seconds for which no player was on the server
	no_player=0

	while true; do
		echo -e "no_player: ${no_player}s\tcheck_player_time: ${CHECK_PLAYER_TIME}s\tidle_if_time: ${IDLE_IF_TIME}s"
		# Retry in ${CHECK_PLAYER_TIME} seconds
		sleep ${CHECK_PLAYER_TIME}

		if screen -S "${SESSION_NAME}" -Q select . > /dev/null; then
			# Game server is up and running
			if [[ "$(screen -S "${SESSION_NAME}" -ls | sed -n "s/.*${SESSION_NAME}\s\+//gp")" == "(Attached)" ]]; then
				# An administrator is connected to the console, pause player checking
				echo "An admin is connected to the console. Pause player checking."
			# Check for active player
			elif SUDO_CMD="" is_player_online; then
				# No player was seen on the server through list
				no_player=$(( no_player + CHECK_PLAYER_TIME ))
				# Stop the game server if no player was active for at least ${IDLE_IF_TIME}
				if [[ "${no_player}" -ge "${IDLE_IF_TIME}" ]]; then
					IDLE_SERVER="false" ${myname} stop
					# Wait for game server to go down
					for i in {1..100}; do
						screen -S "${SESSION_NAME}" -Q select . > /dev/null || break
						[[ $i -eq 100 ]] && echo -e "An \e[39;1merror\e[0m occurred while trying to reset the idle_server!"
						sleep 0.1
					done
					# Reset timer and give the player 300 seconds to connect after pinging
					no_player=$(( IDLE_IF_TIME - 300 ))
					# Game server is down, listen on port ${GAME_PORT} for incoming connections
					echo -n "Netcat: "
					${NETCAT_CMD} -v -l -p ${GAME_PORT} && echo "Netcat caught an connection. The server is coming up again..."
					IDLE_SERVER="false" ${myname} start
				fi
			else
				# Reset timer since there is an active player on the server
				no_player=0
			fi
		else
			# Reset timer and give the player 300 seconds to connect after pinging
			no_player=$(( IDLE_IF_TIME - 300 ))
			# Game server is down, listen on port ${GAME_PORT} for incoming connections
			echo -n "Netcat: "
			${NETCAT_CMD} -v -l -p ${GAME_PORT} && echo "Netcat caught an connection. The server is coming up again..."
			IDLE_SERVER="false" ${myname} start
		fi
	done
}

# Start the server if it is not already running
server_start() {
	# Start the game server
	if ${SUDO_CMD} screen -S "${SESSION_NAME}" -Q select . > /dev/null; then
		echo "A screen ${SESSION_NAME} session is already running. Please close it first."
	else
		echo -en "Starting server..."
		${SUDO_CMD} screen -dmS "${SESSION_NAME}" /bin/bash -c "cd '${SERVER_ROOT}'; ${SERVER_START_CMD}"
		${SUDO_CMD} screen -S "${SESSION_NAME}" -X logfile "${GAME_COMMAND_DUMP}"
		echo -e "\e[39;1m done\e[0m"
	fi

	if [[ "${IDLE_SERVER,,}" == "true" ]]; then
		# Check for the availability of the netcat (nc) binaries
		if [[ -z "${NETCAT_CMD}" ]]; then
			>&2 echo "The netcat binaries are needed for suspending an idle server."
			exit 12
		fi

		# Start the idle server daemon
		if ${SUDO_CMD} screen -S "${IDLE_SESSION_NAME}" -Q select . > /dev/null; then
			${SUDO_CMD} screen -S "${IDLE_SESSION_NAME}" -X quit
			# Restart as soon as the idle_server_daemon has shut down completely
			for i in {1..100}; do
				if ! ${SUDO_CMD} screen -S "${IDLE_SESSION_NAME}" -Q select . > /dev/null; then
					${SUDO_CMD} screen -dmS "${IDLE_SESSION_NAME}" /bin/bash -c "${myname} idle_server_daemon"
					break
				fi
				[[ $i -eq 100 ]] && echo -e "An \e[39;1merror\e[0m occurred while trying to reset the idle_server!"
				sleep 0.1
			done
		else
			echo -en "Starting idle server daemon..."
			${SUDO_CMD} screen -dmS "${IDLE_SESSION_NAME}" /bin/bash -c "${myname} idle_server_daemon"
			echo -e "\e[39;1m done\e[0m"
		fi
	fi
}

# Stop the server gracefully by saving everything prior and warning the users
server_stop() {
	# Quit the idle daemon
	if [[ "${IDLE_SERVER,,}" == "true" ]]; then
		# Check for the availability of the netcat (nc) binaries
		if [[ -z "${NETCAT_CMD}" ]]; then
			>&2 echo "The netcat binaries are needed for suspending an idle server."
			exit 12
		fi

		if ${SUDO_CMD} screen -S "${IDLE_SESSION_NAME}" -Q select . > /dev/null; then
			echo -en "Stopping idle server daemon..."
			${SUDO_CMD} screen -S "${IDLE_SESSION_NAME}" -X quit
			echo -e "\e[39;1m done\e[0m"
		else
			echo "The corresponding screen session for ${IDLE_SESSION_NAME} was already dead."
		fi
	fi

	# Gracefully exit the game server
	if ${SUDO_CMD} screen -S "${SESSION_NAME}" -Q select . > /dev/null; then
		# Game server is up and running, gracefully stop the server when there are still active players

		# Check for active player
		if is_player_online; then
			# No player was seen on the server through playing
			echo -en "Server is going down..."
			game_command exit
		else
			# Player(s) were seen on the server through playing (or an error occurred)
			# Warning the users through the server console
			game_command say "Server is going down in 10 seconds! HURRY UP WITH WHATEVER YOU ARE DOING!"
			game_command save
			echo -en "Server is going down in..."
			for i in {1..10}; do
				game_command say "down in... $(( 10 - i ))"
				echo -n " $(( 10 - i ))"
				sleep 1
			done
			game_command exit
		fi

		# Finish as soon as the server has shut down completely
		for i in {1..100}; do
			if ! ${SUDO_CMD} screen -S "${SESSION_NAME}" -Q select . > /dev/null; then
				echo -e "\e[39;1m done\e[0m"
				break
			fi
			[[ $i -eq 100 ]] && echo -e "\e[39;1m timed out\e[0m"
			sleep 0.1
		done
	else
		echo "The corresponding screen session for ${SESSION_NAME} was already dead."
	fi
}

# Enter the screen game session
server_console() {
	if ${SUDO_CMD} screen -S "${SESSION_NAME}" -Q select . > /dev/null; then
		# Circumvent a permission bug related to running GNU screen as a different user,
		# see e.g. https://serverfault.com/questions/116775/sudo-as-different-user-and-running-screen
		${SUDO_CMD} script -q -c "screen -S \"${SESSION_NAME}\" -rx" /dev/null
	else
		echo "There is no ${SESSION_NAME} session to connect to."
	fi
}

# Help function, no arguments required
help() {
	cat <<-EOF
	This script was designed to easily control any ${game} server. Almost any parameter for a given
	${game} server derivative can be changed by editing the variables in the configuration file.

	Usage: ${myname} {start|stop|restart|status|backup|restore|command <command>|console}
	    start                Start the ${game} server
	    stop                 Stop the ${game} server
	    restart              Restart the ${game} server
	    status               Print some status information
	    backup               Backup the world data
	    restore [filename]   Restore the world data from a backup
	    command <command>    Run the given command at the ${game} server console
	    console              Enter the server console through a screen session

	Copyright (c) Gordian Edenhofer <gordian.edenhofer@gmail.com>
	EOF
}

case "${1:-}" in
	start)
	server_start
	;;

	stop)
	server_stop
	;;

	status)
	server_status
	;;

	restart)
	server_restart
	;;

	console)
	server_console
	;;

	command)
	server_command "${@:2}"
	;;

	backup)
	backup_files
	;;

	restore)
	backup_restore "${@:2}"
	;;

	idle_server_daemon)
	# This shall be a hidden function which should only be invoced internally
	idle_server_daemon
	;;

	-h|--help)
	help
	exit 0
	;;

	*)
	help
	exit 1
	;;
esac

exit 0
