pkgname=terraria-server
pkgver=1.4.2.2
pkgrel=30
pkgdesc="Official dedicated server for Terraria"
arch=('x86_64' 'x86')
license=('unknown')
url="https://terraria.org/"
depends=('mono' 'netcat' 'screen' 'sudo')
makedepends=('unzip')
install='terraria-server.install'

_pkgver=$(echo $pkgver | sed 's/\.//g')

source=(
  "https://terraria.org/system/dedicated_servers/archives/000/000/045/original/terraria-server-1422.zip"
  "https://github.com/mono/reference-assemblies/blob/main/v4.8/System.dll?raw=true"
  "https://github.com/mono/reference-assemblies/blob/main/v4.8/System.Core.dll?raw=true"
  "serverconfig.txt"
  "terrariad.conf"
  "terrariad.service"
  "terrariad.sh"
  "terrariad.sysusers"
  "terrariad.tmpfiles"
  "terrariad-backup.service"
  "terrariad-backup.timer"
)

sha512sums=(
  'd36972f547cfd2859768be058521a8f1f0bcceb3959a09aff6a703599fe3162e7997d7756c8795670f3d630d540a258294bc857736ad440d96fb81d581b26c87'
  '94651de0a4a23922039d997010a7c8d700de30402f262ccab00dc1c992c94f53862e848c3a3f057776c1feba0c55586e05960d379584da30085e1fa4d033c3ce'
  '6693f7ec0b0d98d20bbb2d2e9be0abda0b03909d00d1fddf6dae4d4a3bc77801b04ba4cb93886ee8579e80667bad966bce4f798990587651d9d0286feae74c22'
  'b020fb9f8b2b7a2266300384cbc6ba114cce2d1de9726fa32a27fe761aafae88fb67da16d3ecaec353a89377dc6e6df6ef8a6c4e83208d15f728809e31c2c09c'
  'f0e9b65c9f25097e4462b5245c1f75a7cfc3940cd8f06a75e1c74ca02be0a620daae75f0f782d78839f595c20980a4a01ec028d3ebe1cc83f4c1451ca46f7a26'
  'a1b576b99fb90ff3a93a183b3275f54d0a3b0ffc0e1b313d86026512d1dd87060bfd8f7870de18333d55ae0919a67a64585bd017dd079a19a7fcc620afc56e4e'
  '576ab643ef2e3473d417f1bcb8fbdc3bd6d0c25913daefa5983b0527a5f19a055bf2156921152be4eb5e8a9c41fef2ea1accfa3f01e9ed600da1cf7480096410'
  'a27c1eac379d1665996141a6d9539477a4d12939b10eb3c0514ad60e91cf9eda49de161338d5d24fe208fd82f895f8d5add9d608f0cedb8751dec7d426b973ec'
  '9378e4297c3a6f732acb39fe6b5a0b96f2c0c29279fe71d98ba53f86db09eed9f49fd13d35c0d1dfafbee04f6e4f39962c9052b510230fdec69cfeba9efebc14'
  'a10e38c0e9a09c25e23e46147a8b8ce4d88a62ee780c1c0b525b9e41a563c4a4ed8e94d851abc3936bc31f3faa916ef005543129a039f66878b8f2c34853b91d'
  '19ee3646bfbace353b65c0373594edb654de11c9671f29cebad3b31109f29f94ade1d529d9f409b0989c376bef9b451585b22a1e0ac4295fcc92d9565f808418'
)

_game="terraria"
_server_root="/srv/terraria"

package() {
    unzip -o "${pkgname}-${_pkgver}.zip"

    # Copy the updated dlls to /Linux so that they work with Mono 5.x
    cp -rv "System.dll?raw=true" "${_pkgver}/Linux/System.dll";
    cp -rv "System.Core.dll?raw=true" "${_pkgver}/Linux/System.Core.dll";

    install -o 697 -g 697  -d "${pkgdir}/srv/terraria/"

    cd "${_pkgver}/Linux"
    dest="${pkgdir}/srv/terraria/server-${pkgver}"
    install -o 697 -g 697  -d "${dest}"
	  ln -s "server-${pkgver}" "${pkgdir}/srv/terraria/server"

    install -m644 FNA.dll "${dest}/"
    install -m644 FNA.dll.config "${dest}/"

    install -m644 Mono.Posix.dll "${dest}/"
    install -m644 Mono.Security.dll "${dest}/"

    install -m644 monoconfig "${dest}/"
    install -m644 monomachineconfig "${dest}/"
    install -m644 mscorlib.dll "${dest}/"
    install -m644 open-folder "${dest}/"

    install -m644 System.Configuration.dll "${dest}/"
    install -m644 System.Core.dll "${dest}/"
    install -m644 System.Data.dll "${dest}/"
    install -m644 System.dll "${dest}/"
    install -m644 System.Drawing.dll "${dest}/"
    install -m644 System.Numerics.dll "${dest}/"
    install -m644 System.Runtime.Serialization.dll "${dest}/"
    install -m644 System.Security.dll "${dest}/"
    install -m644 System.Windows.Forms.dll "${dest}/"
    install -m644 System.Windows.Forms.dll.config "${dest}/"
    install -m644 System.Xml.dll "${dest}/"
    install -m644 System.Xml.Linq.dll "${dest}/"

    install -m644 TerrariaServer.exe "${dest}/"
    install -m755 TerrariaServer.bin.${CARCH} "${dest}/"

    install -m644 WindowsBase.dll "${dest}/"

    cd "${srcdir}"
  	install -Dm644 ${_game}d.conf              "${pkgdir}/etc/conf.d/${_game}"
	  install -Dm755 ${_game}d.sh                "${pkgdir}/usr/bin/${_game}d"
	  install -Dm644 ${_game}d.service           "${pkgdir}/usr/lib/systemd/system/${_game}d.service"
	  install -Dm644 ${_game}d-backup.service    "${pkgdir}/usr/lib/systemd/system/${_game}d-backup.service"
	  install -Dm644 ${_game}d-backup.timer      "${pkgdir}/usr/lib/systemd/system/${_game}d-backup.timer"
	  install -Dm644 ${_game}d.sysusers          "${pkgdir}/usr/lib/sysusers.d/${_game}d.conf"
	  install -Dm644 ${_game}d.tmpfiles          "${pkgdir}/usr/lib/tmpfiles.d/${_game}d.conf"

    install -m755 serverconfig.txt "${pkgdir}/srv/terraria/"
    install -o 697 -g 697  -d "${pkgdir}/srv/terraria/worlds/"
}
